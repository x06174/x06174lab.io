'use strict';

function localeDetect() {
  let localeCode = navigator.language || navigator.browserLanguage || navigator.systemLanguage || navigator.userLanguage;
  let locale = localeCode.toLowerCase().substr(0, 2);

  switch (locale) {
    case 'en':
      window.location.href = '/en/';
      break;
    default:
      window.location.href = '/ru/';
  }
}

document.addEventListener('DOMContentLoaded', function () {
  localeDetect();
});
