'use strict';

function bsTooltip() {
  $('[data-toggle="tooltip"]').tooltip();
}

$(function () {
  bsTooltip();
});